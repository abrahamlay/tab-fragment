package com.dev.abrahamlay.tablayout.Util;

import com.dev.abrahamlay.tablayout.Model.Cinema;
import com.dev.abrahamlay.tablayout.Model.City;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Abraham on 10/21/2016.
 */

public interface APIService {
    String URLAPI="http://ibacor.com/api/";
    @GET("jadwal-bioskop")
    Call<City> getListCity();
    @GET("jadwal-bioskop")
    Call<Cinema> getListCinema(@Query("id") String id);

        public static class Factory{
            public static APIService create(){
            Retrofit retrofit= new Retrofit.Builder()
                    .baseUrl(URLAPI)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            return retrofit.create(APIService.class);

        }

    }
}
