package com.dev.abrahamlay.tablayout.Model;

import java.util.List;

/**
 * Created by Abraham on 10/21/2016.
 */

public class City {
    private String status;
    private List<Data> data;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



    public static class Data {
        private String id;
        private String kota;

        public String getKota() {
            return kota;
        }

        public void setKota(String kota) {
            this.kota = kota;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }



    }
}
