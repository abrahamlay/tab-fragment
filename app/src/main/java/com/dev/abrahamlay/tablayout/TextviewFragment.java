package com.dev.abrahamlay.tablayout;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dev.abrahamlay.tablayout.Adapter.CityAdapter;
import com.dev.abrahamlay.tablayout.Model.Cinema;
import com.dev.abrahamlay.tablayout.Model.City;
import com.dev.abrahamlay.tablayout.Util.APIService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class TextviewFragment extends Fragment {

    private APIService apiService;
    private RecyclerView recyclerView;
    private CityAdapter mAdapter;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_textview, container, false);
        apiService= APIService.Factory.create();
        recyclerView = (RecyclerView) view.findViewById(R.id.listKota);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        getCity(getActivity());



        return view;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Retrofit.Builder builder= new Retrofit.Builder();

//        apiService= APIService.Factory.create();

//        city=getCity();
//        mAdapter = new CityAdapter(city);
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
//        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.setAdapter(mAdapter);

    }

    private void getCinema() {
        apiService.getListCinema("32").enqueue(new Callback<Cinema>() {
            @Override
            public void onResponse(Call<Cinema> call, Response<Cinema> response) {
                Cinema film= response.body();
                for (Cinema.Data data :film.getData()){
                    Log.d("film: ",data.getMovie());
                }
            }

            @Override
            public void onFailure(Call<Cinema> call, Throwable t) {
                Log.d("kota: ","error -> "+ t.getMessage().toString());
            }
        });
    }

    private void getCity(final Context context) {
        Call<City> cityCall=apiService.getListCity();
        cityCall.enqueue(new Callback<City>() {
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
                City kota= response.body();
                mAdapter= new CityAdapter(kota,context);
                recyclerView.setAdapter(mAdapter);
                for (City.Data data :kota.getData()){
                    Log.d("kota: ",data.getKota());
                }
            }

            @Override
            public void onFailure(Call<City> call, Throwable t) {
                Log.d("kota: ","error -> "+ t.getMessage().toString());
            }
        });
    }



}
