package com.dev.abrahamlay.tablayout.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dev.abrahamlay.tablayout.Model.City;
import com.dev.abrahamlay.tablayout.R;
import com.dev.abrahamlay.tablayout.Util.APIService;

/**
 * Created by Abraham on 10/23/2016.
 */

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.CityViewHolder> {

    private final City citylist;
    Context context;
    LayoutInflater inflater;
    private APIService apiService;
    public CityAdapter(City cityList, Context context) {
        this.citylist=cityList;
        this.context=context;
        inflater=LayoutInflater.from(context);
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_city,parent,false);
        return new CityViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        City.Data kota=citylist.getData().get(position);
        Log.d ("View holder kota",kota.getKota());
        holder.city.setText(kota.getKota());

    }

    @Override
    public int getItemCount() {
        return citylist.getData().size();
    }

    public class CityViewHolder extends RecyclerView.ViewHolder{

        public TextView city;

        public CityViewHolder(View itemView) {
            super(itemView);

            city = (TextView) itemView.findViewById(R.id.city_name);
        }
    }

}
